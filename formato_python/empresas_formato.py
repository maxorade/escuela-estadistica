#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd

df = pd.read_csv('campo_laboral/empresas_2.csv')

print(df.to_string()) 


# In[3]:


empresas = list()
def addEmpresas(emp):
    if emp not in  empresas:
        empresas.append(emp)
        print("empresa agregada")


# In[27]:



for i in df["Algunas empresas donde se están desempeñando los/as Ingenieros Estadísticos UBB"]:
    part1 = "<tr><td>"
    part2 = "</td></tr>"
    
    addEmpresas(i)


# In[28]:


import pandas as pd

df = pd.read_csv('campo_laboral/exalumnos.csv')


# In[29]:


df["Nombre"]


# In[31]:


for i in range(188):
    part1 = "<tr><td>"
    part2 = "</td></tr>"
    addEmpresas(df["empresa"][i])


# In[42]:


for i in empresas:
    part1 = "<tr><td>"
    part2 = "</td></tr>"
    print(f"{part1}{i}{part2}")


# In[33]:


empresas


# In[9]:


import pandas as pd

df = pd.read_csv('campo_laboral/proyectos.csv')

list(df)


# In[31]:


for i in range(df["Periodo"].size):
    periodo = f"<td>{df['Periodo'][i]}</td>"    
    alumno = f"<td>{df['ALUMNO'][i]}</td>"    
    asignatura = f"<td>{df['ASIGNATURA'][i]}</td>"
    proyecto = f"<td>{df['TITULO PROYECTOS'][i]}</td>"    
    prof = f"<td>{df['PROF. GUIA'][i]}</td>"    
    co = f"<td>{df['CO-GUÍA'][i]}</td>"
    print(f"<tr>{periodo}{alumno}{asignatura}{proyecto}{prof}{co}</tr>")

