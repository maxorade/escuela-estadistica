  </div>
    </div>
    
    <footer class="footer-area">
        <div class="main-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="single-widget pr-60">
                            <div class="footer-logo pb-25">
                                <a href="../index.html"><img src="../img/logo/footer-logo.png" alt="eduhome"></a>
                            </div>
                            
                            <div class="footer-social">
                                <ul>
                                     <li><a href="../https://www.facebook.com/ingestadisticaubb"><i
                                                class="zmdi zmdi-facebook"></i></a></li>
                                    <li><a href="../https://www.instagram.com/estadisticaubb/"><i
                                                class="zmdi zmdi-instagram"></i></a></li>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="single-widget">
                            <h3>Director del Departamento
                            </h3>
                            <br>
                            <p style="text-align: justify">Luis Firinguetti Limone</p>
                            <p style="text-align: justify">lfiringu@ubiobio.cl</p>
                            <p style="text-align: justify"> (41) 311 1723</p>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6 col-xs-12">
                        <div class="single-widget">
                            <h3>Directora de Escuela</h3>
                            <br>
                            <p style="text-align: justify">Nelly Margot Gómez Fuentealba</p>
                            <p style="text-align: justify">ngomez@ubiobio.cl</p>
                            <p style="text-align: justify"> (41) 311 1147</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="single-widget">
                            <h3>Secretaria de Departamento y Escuela:</h3>
                            <p style="text-align: justify">Patricia Toledo Soto</p>
                            <p style="text-align: justify">ptoledo@ubiobio.cl</p>
                            <p style="text-align: justify">(41)3111109</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </footer>
    

    <script src="../js/vendor/jquery-1.12.0.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.meanmenu.js"></script>
    <script src="../js/jquery.magnific-popup.js"></script>
    <script src="../js/ajax-mail.js"></script>
    <script src="../js/owl.carousel.min.js"></script>
    <script src="../js/jquery.mb.YTPlayer.js"></script>
    <script src="../js/jquery.nicescroll.min.js"></script>
    <script src="../js/plugins.js"></script>
    <script src="../js/main.js"></script>
    <script src="../js/lightbox.js"></script>
</body>

</html>