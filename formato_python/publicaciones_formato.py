#!/usr/bin/env python
# coding: utf-8

# In[2]:


#https://pluraldev.com/tools/html-to-json/
#view-source:http://estadistica.ubiobio.cl/?page_id=13
import os, json
import pandas as pd


# In[3]:


path_to_json = 'publicaciones/'
json_files = [pos_json for pos_json in os.listdir(path_to_json) if pos_json.endswith('.json')]


# In[28]:


json_files.reverse() 
json_files


# In[32]:


for file in json_files:
    json_f = pd.read_json('publicaciones/'+ file)
    p1 = "<div class='notice-left-wrapper'><h3>PUBLICACIONES "
    print(p1 + file[0:4] + "</h3><div class='notice-left'>")
    for text in list(json_f[0]):
        p1 = "  <div class='single-notice-left hidden-sm mb-23 pb-20'><p>"
        print(p1 + text + " </p></div>")
    print("</div></div><!-- "+file[0:4]+"  --> <br>")


# In[ ]:




